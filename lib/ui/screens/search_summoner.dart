import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rendu/blocs/summoner_cubit.dart';
import 'package:rendu/models/summoner.dart';
import 'package:rendu/repository/preferences_repository.dart';
import 'package:rendu/repository/repository.dart';
import 'package:rendu/repository/summoners_repository.dart';
import 'package:rendu/ui/helpers/navbar.dart';

class SearchSummoners extends StatefulWidget {
  const SearchSummoners({Key? key}) : super(key: key);

  @override
  _SearchSummonersState createState() => _SearchSummonersState();
}

class _SearchSummonersState extends State<SearchSummoners> {
  final int _selectedIndex = 2;
  final List<Summoner> _summoners = [];
  late Summoner summonerReceived = Summoner('', 0, '', '', '', 0);

  late String summonerIconUrl;
  final SummonerRepository _summonerRepository = SummonerRepository();
  final PreferencesRepository _preferencesRepository = PreferencesRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Padding(
            padding: EdgeInsets.only(top: 4),
            child: Text('SummonersWatcher',
                style: TextStyle(color: Color(0xffc5b88d), fontSize: 24)),
          ),
          backgroundColor: const Color(0xff0E0E0E),
          iconTheme: const IconThemeData(color: Color(0xffc5b88d)),
        ),
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('lib/assets/images/background.jpg'),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                TextField(
                  style: const TextStyle(color: Color(0xffc5b88d)),
                  decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.search, color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(color: Colors.white)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(color: Colors.white)),
                      hintText: 'Search a summoner by his name',
                      hintStyle: TextStyle(color: Colors.white),
                      labelText: 'Summoner\'s name',
                      labelStyle: TextStyle(color: Colors.white)),
                  onSubmitted: (query) async {
                    summonerReceived = await Repository(
                            _summonerRepository, _preferencesRepository)
                        .searchSummoner(query);
                    summonerIconUrl = summonerReceived.profileIconId.toString();
                    setState(() {
                      _summoners.clear();
                      _summoners.add(summonerReceived);
                    });
                  },
                ),
                summonerReceived.name != ''
                    ? SearchSummoner(summonerReceived, summonerIconUrl)
                    : const NoSummoners(),
                summonerReceived.name != ''
                    ? ButtonSummoner(summonerReceived)
                    : const ButtonNoSummoner()
              ],
            ),
          ),
        ),
        bottomNavigationBar: Navbar(_selectedIndex));
  }
}

class SearchSummoner extends StatelessWidget {
  const SearchSummoner(this.summonerReceived, this.summonerIconUrl, {Key? key})
      : super(key: key);
  final Summoner summonerReceived;
  final String summonerIconUrl;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 8,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ListTile(
              title: const Center(
                  child: Padding(
                padding: EdgeInsets.all(1.0),
                child: Text('Summoner\'s name :',
                    style: TextStyle(color: Colors.white)),
              )),
              subtitle: Center(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(summonerReceived.name,
                    style: const TextStyle(
                        fontSize: 30, color: Color(0xffc5b88d))),
              ))),
          ListTile(
              title: const Center(
                  child: Padding(
                padding: EdgeInsets.all(1.0),
                child: Text('Summoner\'s level :',
                    style: TextStyle(color: Colors.white)),
              )),
              subtitle: Center(
                  child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Text(summonerReceived.summonerLevel.toString(),
                    style: const TextStyle(
                        fontSize: 30, color: Color(0xffc5b88d))),
              ))),
          Container(
            width: 250,
            height: 250,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                image: DecorationImage(
                    image: NetworkImage(
                        'http://ddragon.leagueoflegends.com/cdn/12.3.1/img/profileicon/$summonerIconUrl.png'),
                    fit: BoxFit.fill)),
          )
        ],
      ),
    );
  }
}

class NoSummoners extends StatelessWidget {
  const NoSummoners({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30, bottom: 30),
          child: Image.network(
              'https://static.wikia.nocookie.net/leagueoflegends/images/9/99/Bee_Sad_Emote.png/revision/latest/scale-to-width-down/250?cb=20180508204109'),
        ),
        const Text(
          'No summoner was found.',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ],
    ));
  }
}

class ButtonSummoner extends StatelessWidget {
  const ButtonSummoner(this._summonerReceived, {Key? key}) : super(key: key);
  final Summoner _summonerReceived;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Container(
        height: 70,
        width: 400,
        child: Center(
            child: Padding(
          padding: const EdgeInsets.only(left: 20, top: 5),
          child: Text(
            'Follow ' + _summonerReceived.name,
            style: const TextStyle(color: Colors.white, fontSize: 20),
          ),
        )),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('lib/assets/images/buttonAdd.png'),
                fit: BoxFit.fitWidth)),
      ),
      onPressed: () {
        Summoner summoner = Summoner(
            _summonerReceived.accountId,
            _summonerReceived.profileIconId,
            _summonerReceived.name,
            _summonerReceived.id,
            _summonerReceived.puuid,
            _summonerReceived.summonerLevel);
        Provider.of<SummonerCubit>(context, listen: false)
            .addSummoner(summoner);
        Navigator.of(context).pushNamed('/show', arguments: summoner);
      },
    );
  }
}

class ButtonNoSummoner extends StatelessWidget {
  const ButtonNoSummoner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Container(
        height: 70,
        width: 400,
        child: const Center(
            child: Padding(
          padding: EdgeInsets.only(left: 20, top: 5),
          child: Text(
            'Go back',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )),
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('lib/assets/images/buttonArrow.png'),
                fit: BoxFit.fitWidth)),
      ),
      onPressed: () {
        Navigator.of(context).pushNamed('/show');
      },
    );
  }
}
