import 'package:flutter/material.dart';
import 'package:rendu/ui/helpers/navbar.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final int _selectedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Padding(
          padding: EdgeInsets.only(top: 4),
          child: Text('SummonersWatcher',
              style: TextStyle(color: Color(0xffc5b88d), fontSize: 24)),
        ),
        backgroundColor: const Color(0xff0E0E0E),
        iconTheme: const IconThemeData(color: Color(0xffc5b88d)),
      ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('lib/assets/images/background.jpg'),
                fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 100),
              child: Image.asset('lib/assets/images/logo.png'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: MaterialButton(
                child: Container(
                  height: 70,
                  width: 400,
                  child: const Center(
                      child: Padding(
                    padding: EdgeInsets.only(left: 20, top: 5),
                    child: Text(
                      'Follow new summoners',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  )),
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('lib/assets/images/buttonAdd.png'),
                          fit: BoxFit.fitWidth)),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed('/add');
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: MaterialButton(
                child: Container(
                  height: 70,
                  width: 400,
                  child: const Center(
                      child: Padding(
                    padding: EdgeInsets.only(left: 20, top: 5),
                    child: Text(
                      'Your summoners',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  )),
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('lib/assets/images/buttonLike.png'),
                          fit: BoxFit.fitWidth)),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed('/show');
                },
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Navbar(_selectedIndex),
    );
  }
}
