import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rendu/blocs/summoner_cubit.dart';
import 'package:rendu/models/summoner.dart';
import 'package:rendu/ui/helpers/navbar.dart';

class ShowSummoners extends StatelessWidget {
  const ShowSummoners({Key? key}) : super(key: key);
  final int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Padding(
            padding: EdgeInsets.only(top: 4),
            child: Text('SummonersWatcher',
                style: TextStyle(color: Color(0xffc5b88d), fontSize: 24)),
          ),
          backgroundColor: const Color(0xff0E0E0E),
          iconTheme: const IconThemeData(color: Color(0xffc5b88d)),
        ),
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('lib/assets/images/background.jpg'),
                  fit: BoxFit.cover)),
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: BlocBuilder<SummonerCubit, List<Summoner>>(
                    builder: (context, state) {
                      return ListView.builder(
                          itemCount: state.length,
                          itemBuilder: (BuildContext context, int index) {
                            Summoner summoner = state[index];
                            final summonerIcon =
                                summoner.profileIconId.toString();
                            return Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10, bottom: 10),
                                  child: Card(
                                    child: ListTile(
                                      leading: Image.network(
                                          'http://ddragon.leagueoflegends.com/cdn/12.3.1/img/profileicon/$summonerIcon.png'),
                                      title: Text(
                                        summoner.name,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Text('Summoner\'s level: ' +
                                          summoner.summonerLevel.toString()),
                                      trailing: IconButton(
                                        onPressed: () {
                                          state.removeAt(index);
                                          Provider.of<SummonerCubit>(context,
                                                  listen: false)
                                              .saveSummoner(summoner);
                                        },
                                        icon: const Icon(Icons.clear,
                                            color: Colors.black),
                                      ),
                                    ),
                                    elevation: 8,
                                  ),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Navbar(_selectedIndex));
  }
}
