import 'package:flutter/material.dart';

class Navbar extends StatefulWidget {
  const Navbar(this.selectedIndex, {Key? key}) : super(key: key);
  final int selectedIndex;

  @override
  State<Navbar> createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  @override
  Widget build(BuildContext context) {
    void _onItemTapped(int index) {
      switch (index) {
        case 0:
          {
            Navigator.of(context).pushNamed('/show');
          }
          break;
        case 1:
          {
            Navigator.of(context).pushNamed('/home');
          }
          break;
        case 2:
          {
            Navigator.of(context).pushNamed('/add');
          }
          break;
      }
    }

    return BottomNavigationBar(
      backgroundColor: const Color(0xff0E0E0E),
      selectedItemColor: const Color(0xffc5b88d),
      unselectedItemColor: Colors.white,
      showSelectedLabels: false,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite),
          label: 'Followed',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.house_rounded),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.add),
          label: 'Add',
        ),
      ],
      currentIndex: widget.selectedIndex,
      onTap: _onItemTapped,
    );
  }
}
