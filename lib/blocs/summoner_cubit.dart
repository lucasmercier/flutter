import 'package:bloc/bloc.dart';
import 'package:rendu/models/summoner.dart';
import 'package:rendu/repository/repository.dart';

class SummonerCubit extends Cubit<List<Summoner>> {
  SummonerCubit(this._repository) : super([]);
  final Repository _repository;

  void addSummoner(Summoner summoner) {
    emit([...state, summoner]);
    _repository.saveSummoners(state);
  }

  Future<void> saveSummoner (Summoner summoner) async {
    _repository.saveSummoners(state);
    loadSummoners();
  }

  Future<void> loadSummoners () async {
    final List<Summoner> summoners = await _repository.loadSummoners();
    emit(summoners);
  }
}