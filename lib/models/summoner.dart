import 'dart:convert';

class Summoner {
  String accountId;
  int profileIconId;
  String name;
  String id;
  String puuid;
  int summonerLevel;

  String toJson() {
    return jsonEncode({
      'accountId': accountId,
      'profileIconId': profileIconId,
      'name': name,
      'id': id,
      'puuid': puuid,
      'summonerLevel': summonerLevel
    });
  }

  factory Summoner.fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Summoner(
        map['accountId'],
        map['profileIconId'],
        map['name'],
        map['id'],
        map['puuid'],
        map['summonerLevel'],
    );
  }

  factory Summoner.fromFactoryJson(Map<String, dynamic> json) {
    String accountId = json['accountId'];
    int profileIconId = json['profileIconId'];
    String name = json['name'];
    String id = json['id'];
    String puuid = json['puuid'];
    int summonerLevel = json['summonerLevel'];

    return Summoner(accountId, profileIconId, name, id, puuid, summonerLevel);
  }

  Summoner(this.accountId, this.profileIconId, this.name, this.id, this.puuid, this.summonerLevel);
}