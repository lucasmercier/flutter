import 'package:shared_preferences/shared_preferences.dart';
import 'package:rendu/models/summoner.dart';

class PreferencesRepository {

  Future<void> saveSummoners(List<Summoner> summoners) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> myList = summoners.map((Summoner summoner) {
      return summoner.toJson();
    }).toList();
    prefs.setStringList('summoners', myList);
  }

  Future<List<Summoner>> loadSummoners() async {
    SharedPreferences? prefs = await SharedPreferences.getInstance();
    List<String>? prefString = prefs.getStringList("summoners");
    List<Summoner> summonersSaved = [];
    if (prefString != null) {
      for (var pref in prefString) {
        summonersSaved.add(Summoner.fromJson(pref));
      }
    }
    return summonersSaved;
  }
}