import 'package:rendu/constants.dart';
import 'package:rendu/models/summoner.dart';
import 'package:http/http.dart';

class SummonerRepository {
  Future<Summoner> fetchSummoner(String query) async {
    Response response = await get(Uri.https('euw1.api.riotgames.com',
        '/lol/summoner/v4/summoners/by-name/$query', {
          'api_key' : apiKey
        }));
    if (response.statusCode == 200) {
      final summoner = Summoner.fromJson(response.body);
      return summoner;
    } else {
      return Summoner('', 0, '', '', '', 0);
    }
  }
}

