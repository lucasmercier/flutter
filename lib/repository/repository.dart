import 'package:rendu/models/summoner.dart';
import 'package:rendu/repository/preferences_repository.dart';

import 'summoners_repository.dart';

class Repository {
  final SummonerRepository _summonerRepository;
  final PreferencesRepository _preferencesRepository;

  Repository(this._summonerRepository, this._preferencesRepository);

  Future<Summoner> searchSummoner(String query) {
    return SummonerRepository().fetchSummoner(query);
  }

  Future<void> saveSummoners(List<Summoner> summoners) async {
    _preferencesRepository.saveSummoners(summoners);
  }

  Future<List<Summoner>> loadSummoners() async =>_preferencesRepository.loadSummoners();
}