Riot game étant tatillon sur les bords, on est obligé de rentrer en param des call api une clé qui n'est valide que 24h. Il faudra donc en générer une sur le site :

https://developer.riotgames.com/

J'ai créé un compte exprès pour l'occasion : 

login : mdsFlutterApiAccount  
mot de passe : sV);C?3f7A9QWeR  

Remplir le capchat anti spam et cliquer sur régénérer la clé.

Dans le fichier constants.dart a la racine du projet, remplacer la clé présente par la nouvelle.

Fonctionnalitées :

La page home avec deux boutons :
- Follow new summoners qui permet de lancer une recherche d'un summoner dans via l'api de riot games.
- Your summoners qui permet d'accéder à la liste des summoners que l'on a recherché et suivi.

Exemple de recherche donnant un résultat : 

    Chevreuil (moi), 
    Nivrik (un ami), 
    ZeldaHopeless (Déborah), 
    Hahfrul (Jeremy), 
    Maudit72 (un pseudo ridicule), 
    Do god complex (un autre ami)

La page Follow summoners qui comporte un champ dans lequel on va renseigner le nom du joueur. Le nom doit être exact sinon l'api ne renverra rien. (Ce n'est pas l'api la plus interessante dans la mesure ou l'on doit envoyer un et seul String qui se doit d'être exact mais pour la suite de ce que je compte faire (plus tard hors cours) c'est ce qu'il me faut.)

Si rien n'est trouvé et qu'on en a marre on peut revenir en arriere avec le bouton go back. Si un résultat est trouvé l'identité du joueur s'affiche. on peut alors le follow et accéder a la page des favoris.

La page des favoris liste simplement les infos des joueurs suivis. Par la suite je compte ouvrir une page détailé au tap sur une tuile, éxécutant un d'autre calls d'autres apis a partir de l'id du joueur qui permettra d'avoir des statistiques détaillées, mais c'est très long et fastidieux.

Une barre de navigation est présente en bas de page afin de pouvoir naviguer plus facilement entre les 3 onglets présents pour le moment.


